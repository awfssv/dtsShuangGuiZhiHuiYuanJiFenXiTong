



var cookie = {
    Get: function (name) {
        var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
        if (arr = document.cookie.match(reg)) {
            return unescape(arr[2])
        } else {
            return null
        }
    },
    Del: function (name) {
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval = this.Get(name);
        if (cval != null) {
            document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString()
        }
    },
    Set: function (name, value, time) {
        time = time || "h1";
        var strsec = this._getsec(time);
        var exp = new Date();
        exp.setTime(exp.getTime() + strsec * 1);
        document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";path=/";
    },
    _getsec: function (str) {
        var str1 = str.substring(1, str.length) * 1;
        var str2 = str.substring(0, 1);
        if (str2 == "s") {
            return str1 * 1000
        } else {
            if (str2 == "h") {
                return str1 * 60 * 60 * 1000
            } else {
                if (str2 == "d") {
                    return str1 * 24 * 60 * 60 * 1000
                }
            }
        }
    }
};



var ajaxServer = {
    post: function (obj, fn) {
        var url = "/api/server/post/" + obj.method;
        $.ajax({
            type:"POST",
            url: url,
            contentType : "application/json",
            data: JSON.stringify( obj),
            success: fn
        });
    },
    get: function (obj, fn) {
        var url = "/api/server/get/" + obj.method;
        obj.params && ( url = url + "?params=" + encodeURI(JSON.stringify(obj.params))) || "";
        $.get(url, fn)
    }
}


function DataComp(id, type) {
  var a = $("#" + id ).find("input,textarea,select"),b=[],c;
    if (type && (type.toLowerCase() === "json")) {
        for (var n = 0; n < a.length; n++) {
            c=a[n];
            ( c.value  && c.name ) && (
                isNaN(parseInt(c.value) )
                ?
                b.push('"' + c.name.toLowerCase() + '":"' + c.value + '"')
                :
                b.push('"' + c.name.toLowerCase() + '":' + c.value))
        }
        return "{" + b.join(",") + "}"
    }

    for (var n = 0; n < a.length; n++) {
        c=a[n]
        b.push( c.name.toLowerCase() + "=" + c.value)
    }
    return b.join("&")
}

function activeMenu(){
    var a=location.href,b=a.split('?')[0];
    b=b.split('\/');
    b=b[b.length-1];
    $('nav.header-navi a').removeClass('cur');
    $('[href$="'+ b +'"]').addClass('cur');
}
