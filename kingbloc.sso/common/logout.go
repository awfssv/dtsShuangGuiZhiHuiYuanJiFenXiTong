package common

import (
	"kingbloc.sso/user"

	"net/http"

	"kingbloc.util/util"
)

func Logout(res http.ResponseWriter, req *http.Request) {
	token, _ := req.Cookie("token")
	if token == nil {
		util.WriteJSON(res, "OK")
		return
	}
	state := new(user.State)
	state.Token = token.Value
	state.Del()
	util.WriteJSON(res, "OK")
}



