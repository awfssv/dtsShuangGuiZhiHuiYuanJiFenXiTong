-- MySQL dump 10.13  Distrib 5.5.50, for osx10.8 (i386)
--
-- Host: localhost    Database: sso_dev
-- ------------------------------------------------------
-- Server version	5.5.50

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `token` varchar(255) DEFAULT NULL,
  `userjson` varchar(255) DEFAULT NULL,
  `overdue` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES ('194064d1-6bf1-40db-ad7f-d803fc2200cf','18620131415_1','2017-10-21 17:38:56');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `q_q` int(11) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(8) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `alipay` varchar(50) DEFAULT NULL,
  `wechat` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `identity` varchar(20) DEFAULT NULL,
  `create` datetime DEFAULT NULL,
  `last` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,611041314,1,'18620131415','0b14728a','18620131415','18620131415','18620131415@3737.io','18620131415','18620131415@3737.io','北京','中关村051号','3737.io','2017-10-21 11:29:44','2017-10-21 11:29:44'),(2,611041314,1,'test','0b14728a','简洁的代言','13790226216','13790226216@qq.com','heimawangzi_com','13790226216@3737.io','北京','中关村055号','6688','2017-10-21 11:29:44','2017-10-21 11:29:44'),(3,611041314,1,'17750662398','0b14728a','千山万水','17750662398','17750662398@3737.io','17750662398','17750662398@3737.io','北京','中关村01号','3737.io','2017-10-21 11:29:44','2017-10-21 11:29:44'),(4,611041314,1,'18059244379','0b14728a','18059244379','18059244379','18059244379@3737.io','18059244379','18059244379@3737.io','北京','中关村051号','3737.io','2017-10-21 11:29:44','2017-10-21 11:29:44'),(5,611041314,1,'18650710067','0b14728a','18650710067','18650710067','18650710067@3737.io','18650710067','18650710067@3737.io','北京','中关村051号','3737.io','2017-10-21 11:29:44','2017-10-21 11:29:44');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-21 16:31:02
