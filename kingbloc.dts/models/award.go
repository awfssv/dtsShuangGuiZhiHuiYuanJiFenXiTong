package model

import (
	"kingbloc.util/util"
	"strings"
	"time"
)

// 层奖记录
type LayerAward struct {
	Id                            int64
	Layer                         int
	CreateTime                    time.Time `json:"createTime"`
	Price                         float64
	IncomeOrderId                 int64 `json:"incomeOrderId"`
	LeftAreaOrderId               int64 `json:"leftAreaOrderId"`
	LeftRegionalReferenceOrderId  int64 `json:"leftRegionalReferenceOrderId" xorm:"left_regional_ref"`
	RightAreaOrderId              int64 `json:"rightAreaOrderId"`
	RightRegionalReferenceOrderId int64 `json:"rightRegionalReferenceOrderId" xorm:"right_regional_ref"`
}

//: new 层碰实例指针
func NewLayerAward(incomeOrderId, leftAreaOrderId, leftRegionalRef int64, layer int) *LayerAward {
	la := new(LayerAward)
	la.CreateTime = time.Now()
	la.IncomeOrderId = incomeOrderId
	la.LeftAreaOrderId = leftAreaOrderId
	la.LeftRegionalReferenceOrderId = leftRegionalRef
	la.Layer = layer
	return la
}

// 受益order id 指定层的受益记录
func (laa *LayerAward) ExistLeftAreaOrderIdLA(incomeOrderId int64, layer int) *LayerAward {
	la := new(LayerAward)
	//_, err := MysqlEngine.Where("income_order_id = ?", incomeOrderId).And("layer = ?", layer).Get(la)
	_, err := MysqlEngine.Where("income_order_id =? and layer=?", incomeOrderId, layer).Get(la)
	util.CheckErr(err)
	return la
}

// 所有的层奖
func (la *LayerAward) AllLayerAward() []*LayerAward {
	las := make([]*LayerAward, 0)
	err := MysqlEngine.AllCols().Find(&las)
	util.CheckErr(err)
	return las

}

// 满足计算条件，并且没有计算的层奖
func (la *LayerAward) AllNotCalcLayerAward() []*LayerAward {
	las := make([]*LayerAward, 0)
	err := MysqlEngine.Where("Price = 0 and left_area_order_id > 0 and right_area_order_id > 0  ").Find(&las)
	util.CheckErr(err)
	return las

}

func (la *LayerAward) HasLayerAwardOrder(incomeOrder, orderId int64) bool {
	laa := new(LayerAward)
	MysqlEngine.Sql("select id from layer_award where income_order_id = ? and (left_area_order_id  = ? or right_area_order_id  = ? or left_regional_ref = ? or right_regional_ref = ?)", incomeOrder, orderId, orderId, orderId, orderId).Get(laa)
	if laa.Id > 0 {
		return true
	}
	return false
}

func (la *LayerAward) Update() {
	_, err := MysqlEngine.Id(la.Id).Update(la)
	util.CheckErr(err)
}

// 修改右区层碰的 order id
func (la *LayerAward) UpdateRR() {
	_, err := MysqlEngine.Id(la.Id).Cols("right_area_order_id", "right_regional_ref").Update(la)
	util.CheckErr(err)

}

// 添加一条层碰数据
func (la *LayerAward) Add() {
	_, err := MysqlEngine.InsertOne(*la)
	util.CheckErr(err)
}

// 量奖记录
// 量奖记录
// 量奖记录
// 量奖记录
// 量奖记录
// 量奖记录
// 量奖记录
type AmountAward struct {
	Id                int64     `json:"id"`
	UpdateTime        time.Time `json:"createTime"`
	LeftPrice         float64   `json:"leftPrice"`         // 计算出金额，就不用在计算了
	RightPrice        float64   `json:"rightPrice"`        // 计算出金额，就不用在计算了
	IncomeOrderId     int64     `json:"incomeOrderId"`     // 量的受益者 order id
	LeftAreaOrderIds  string    `json:"leftAreaOrderIds"`  // 多个ID , 号分割
	RightAreaOrderIds string    `json:"rightAreaOrderIds"` // 多个ID , 号分割
	LeftIdsBackup     string    `json:"-"`
	RightIdsBackup    string    `json:"-"`
}

func NewAmountAward(incomeOrderId int64) *AmountAward {
	aa := new(AmountAward)
	aa.IncomeOrderId = incomeOrderId
	aa.UpdateTime = time.Now()
	return aa

}

func (aa *AmountAward) ByIncomeOrderId(id int64) *AmountAward {
	MysqlEngine.Where("income_order_id = ?", id).Get(aa)
	if aa.Id == 0 {
		return nil
	}
	return aa

}

func (aa *AmountAward) AllNotCalcAA() []*AmountAward {
	aas := make([]*AmountAward, 0)
	err := MysqlEngine.Where("left_area_order_ids != '' ").And("right_area_order_ids != '' ").Find(&aas)
	util.CheckErr(err)
	return aas

}

func (aa *AmountAward) Add() {
	_, err := MysqlEngine.InsertOne(aa)
	util.CheckErr(err)
}
func (aa *AmountAward) UpdateBack() {
	_, err := MysqlEngine.Id(aa.Id).Cols("left_price", "right_price", "left_area_order_ids", "right_area_order_ids", "left_ids_backup", "right_ids_backup", "update_time").Update(aa)
	util.CheckErr(err)
}

func (aa *AmountAward) UpdateLeftAreaIds(leftAreaId string) {
	if aa.HasIdExtisLeftRightArea(leftAreaId) {
		return
	}
	aa.LeftAreaOrderIds = aa.LeftAreaOrderIds + "," + leftAreaId
	aa.UpdateTime = time.Now()
	_, err := MysqlEngine.Id(aa.Id).Cols("left_area_order_ids", "update_time").Update(aa)
	util.CheckErr(err)
}

func (aa *AmountAward) UpdateRightAreaIds(leftAreaId string) {
	if aa.HasIdExtisLeftRightArea(leftAreaId) {
		return
	}
	aa.RightAreaOrderIds = aa.RightAreaOrderIds + "," + leftAreaId
	aa.UpdateTime = time.Now()
	_, err := MysqlEngine.Id(aa.Id).Cols("right_area_order_ids", "update_time").Update(aa)
	util.CheckErr(err)
}

func (aa *AmountAward) HasIdExtisLeftRightArea(id string) bool {
	originIds := aa.LeftIdsBackup + "," + aa.RightIdsBackup + "," + aa.LeftAreaOrderIds + "," + aa.RightAreaOrderIds
	ids := strings.Split(originIds, ",")

	for _, v := range ids {
		if v == "" {
			continue
		}
		if id == v {
			return true
		}
	}

	return false
}

func (aa *AmountAward) HasIdExtisLeftRightAreaBack(id string) bool {
	originIds := aa.LeftIdsBackup + "," + aa.RightIdsBackup
	ids := strings.Split(originIds, ",")

	for _, v := range ids {
		if v == "" {
			continue
		}
		if id == v {
			return true
		}
	}

	return false
}

func (aa *AmountAward) UpdatePrice(leftPrice, rightPrice float64) {
	aa.LeftPrice = leftPrice
	aa.RightPrice = rightPrice
	aa.UpdateTime = time.Now()
	_, err := MysqlEngine.Id(aa.Id).Cols("left_price", "right_price", "update_time").Update(aa)
	util.CheckErr(err)
}

func (aa *AmountAward) FindByIncomeOrderId(incomeOrderId int64) *AmountAward {
	_, err := MysqlEngine.Where("income_order_id=?", incomeOrderId).Get(aa)
	util.CheckErr(err)
	if aa.Id > 0 {
		return aa
	}
	return nil
}

// 幸运奖记录
// 幸运奖记录
// 幸运奖记录
// 幸运奖记录
// 幸运奖记录
// 幸运奖记录
// 幸运奖记录
type LuckyAward struct {
	Id                   int64
	AmountPrice          float64   // 触发单当前量的金额
	TriggerOrderId       int64     `json:"triggerOrderId"` // 触发者id 也就是 量奖的受益者 order id //
	TriggerOrderIdBackup int64     // 计算后将TriggerOrderId 放到这里，作为备份用，
	CalcTime             time.Time // 计算时间
}

func NewLuckyAward() *LuckyAward {
	la := new(LuckyAward)
	return la
}

func (la *LuckyAward) AllNotCalc() []*LuckyAward {
	datas := make([]*LuckyAward, 0)
	MysqlEngine.Where("trigger_order_id > 0").Find(&datas)
	return datas
}

func (la *LuckyAward) Add() {
	if la.HasNotExsits() {
		MysqlEngine.InsertOne(la)
	}
}

func (la *LuckyAward) HasNotExsits() bool {
	MysqlEngine.Where("trigger_order_id=?", la.TriggerOrderId).Get(la)
	if la.Id > 0 {
		return false
	}
	return true
}

func (la *LuckyAward) Update() {

	MysqlEngine.Id(la.Id).Cols("trigger_order_id", "trigger_order_id_backup", "calc_time").Update(la)

}

func (la *LuckyAward) Del() {
	MysqlEngine.Where("trigger_order_id=?", la.TriggerOrderId).Delete(la)
}

// 管理奖
// 管理奖
// 管理奖
// 管理奖
// 管理奖
// 管理奖
type ManagementAward struct {
	Id                   int64
	AmountPrice          float64   // 触发单当前量的金额
	TriggerOrderId       int64     `json:"triggerOrderId"` // 触发单号的id
	TriggerOrderIdBackup int64     // 计算后将TriggerOrderId 放到这里，作为备份用，
	CalcTime             time.Time // 计算时间
}

func NewMangementAward() *ManagementAward {
	ma := new(ManagementAward)
	return ma
}

func (la *ManagementAward) AllNotCalc() []*ManagementAward {
	datas := make([]*ManagementAward, 0)
	MysqlEngine.Where("trigger_order_id > 0").Find(&datas)
	return datas
}

func (la *ManagementAward) Update() {
	MysqlEngine.Id(la.Id).Cols("trigger_order_id", "trigger_order_id_backup", "calc_time").Update(la)
}
func (ma *ManagementAward) Add() {
	if ma.HasNotExsits() {

		MysqlEngine.InsertOne(ma)
	}
}

func (ma *ManagementAward) HasNotExsits() bool {
	MysqlEngine.Where("trigger_order_id", ma.TriggerOrderId).Get(ma)
	if ma.Id > 0 {
		return false
	}
	return true
}

func (ma *ManagementAward) Del() {
	MysqlEngine.Where("trigger_order_id=?", ma.TriggerOrderId).Delete(ma)
}

// 魅力奖
type CharmAward struct {
	Id                 int64
	Price              float64   // 奖
	CalcTime           time.Time // 计算时间
	IncomeOrderId      int64
	IntroducerIdsCount int
}

func NewCharmAward() *CharmAward {
	cc := new(CharmAward)
	cc.CalcTime = time.Now()
	return cc
}

func (c *CharmAward) Add() {
	MysqlEngine.InsertOne(c)

}
